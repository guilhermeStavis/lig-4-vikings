//Cria e printa o tabuleiro na tela.
function criarTabuleiro() {
    let jogada = document.createElement("img");
    for (let i = 0; i < 7; i++) {
        const colunas = document.createElement('div')
        colunas.id = `coluna${i}`
        colunas.className = 'vertical'
        document.getElementById('grid').appendChild(colunas)

        for (let j = 0; j < 6; j++) {
            const linhas = document.createElement('div')
            linhas.id = `linha${j}`
            linhas.className = `linha`
            jogada = document.createElement("img");
            linhas.appendChild(jogada);
            document.getElementById(`coluna${i}`).appendChild(linhas)
        }
    }
}

//Verifica se a vez é do jogador V(vermelho) ou P(preto).
function verificarVezJogador(tabuleiro) {
    let contarJ1 = 0;
    let contarJ2 = 0;
    for (let linha = 0; linha < tabuleiro.length; linha++) {
        for (let coluna = 0; coluna < tabuleiro[0].length; coluna++) {
            if (tabuleiro[linha][coluna] === "J1") {
                contarJ1++;
            }
            if (tabuleiro[linha][coluna] === "J2") {
                contarJ2++;
            }
        }
    }
    if (contarJ1 === contarJ2) {
        return "J1";
    } else {
        return "J2";
    }
}

//Cria a matriz relacionada ao tabuleiro, que é utilizada para verificar a vitória.
function criarMatrizTabuleiro() {
    let tabuleiro = [
        ["", "", "", "", "", "", ""],
        ["", "", "", "", "", "", ""],
        ["", "", "", "", "", "", ""],
        ["", "", "", "", "", "", ""],
        ["", "", "", "", "", "", ""],
        ["", "", "", "", "", "", ""]
    ];

    const elementoGrid = document.getElementById("grid");
    let quantidadeColunas = elementoGrid.children.length;
    let quantidadeLinhas = elementoGrid.children[0].children.length;

    for (let coluna = 0; coluna < quantidadeColunas; coluna++) {
        for (let linha = 0; linha < quantidadeLinhas; linha++) {
            if (elementoGrid.children[coluna].children[linha].children[0].classList[0] !== undefined) {
                tabuleiro[linha][coluna] = elementoGrid.children[coluna].children[linha].children[0].classList[0];
            }
        }
    }
    return tabuleiro;
}

//É a função para tratar o evento do clique do jogador.
function selecaoDisco(evt) {
    let jogadorVez = verificarVezJogador(criarMatrizTabuleiro()); //Pega o Jogador atual
    let objJogada = {}; //Objeto que pegará a linha e coluna da posição do ultima peça jogada.
    let elementoColuna; // pega a coluna clicada

    //Verifica qual elemento o target pegou
    if (evt.target.id.slice(0, -1) === "linha") {
        elementoColuna = evt.target.parentNode;
    } else {
        elementoColuna = evt.target.parentNode.parentNode;
    }

    //Verifica se uma coluna foi selecionada
    if (elementoColuna.id.slice(0, -1) === "coluna") {
        console.log("Elemento selecionado: " + elementoColuna.id)
        quantidadeFilhos = elementoColuna.children.length; // Pega a quantidade de filhos em uma coluna.
        let colunaSelecionada = parseInt(elementoColuna.id.replace("coluna", "")); //Pega o número da coluna selecionada.
        console.log("Quantidade de filhos da coluna: " + elementoColuna.children.length);

        //Varre a coluna de baixo para cima para verificar onde a peça vai ficar
        for (let i = quantidadeFilhos - 1; i >= 0; i--) {
            console.log(elementoColuna.children[i]);
            if (elementoColuna.children[i].children[0].classList.length === 0) {
                elementoColuna.children[i].children[0].className += jogadorVez + " animar";
                if(jogadorVez === "J1"){
                    elementoColuna.children[i].children[0].setAttribute("src","img/gema-verde.png");
                }else{
                    elementoColuna.children[i].children[0].setAttribute("src","img/gema-azul.png");
                }
                objJogada.linha = i;
                objJogada.coluna = colunaSelecionada;
                break;
            }
        }

        //Verifica a vitória e caso seja alcançada, remove o evento
        if (Object.keys(objJogada).length !== 0) {
            const gridTabuleiro = document.getElementById("grid");
            const exibirResultado = document.getElementById("resultado");
            if (verificarVitoria(criarMatrizTabuleiro(), objJogada) === true) {
                if (jogadorVez === "J1") {
                    exibirResultado.textContent = "Gema verde venceu!";
                } else {
                    exibirResultado.textContent = "Gema azul venceu!";
                }

                gridTabuleiro.removeEventListener("click", selecaoDisco);
            } else {
                let tabuleiro = criarMatrizTabuleiro()
                let contadorMarcado = 0
                for (let i = 0; i < tabuleiro.length; i ++) {
                    for (let j = 0; j < tabuleiro[i].length; j++) {
                        if (tabuleiro[i][j] !== '') {
                            contadorMarcado++
                        }
                    }
                }

                if (contadorMarcado === 42) {
                    exibirResultado.textContent = 'Empate!'
                }
            }
        }
    }
}

//Limpa o grid em caso de reiniciar o jogo
function limparJogo() {
    const gridTabuleiro = document.getElementById("grid");
    const containerResultado = document.getElementById("resultado");
    containerResultado.textContent = "";
    gridTabuleiro.innerHTML = "";
}

//Função que chama o reinício do jogo. Além de reiniciar, ela elimina o esventos ativos.
function reiniciarJogo() {
    const gridTabuleiro = document.getElementById("grid");
    const botaoReiniciar = document.getElementById("bntReiniciar");

    botaoReiniciar.removeEventListener("click", reiniciarJogo);
    gridTabuleiro.removeEventListener("click", selecaoDisco);

    iniciarJogo();
}

//Função para iniciar o jogo
function iniciarJogo() {
    const gridTabuleiro = document.getElementById("grid");
    const botaoReiniciar = document.getElementById("bntReiniciar");

    limparJogo();
    criarTabuleiro();

    gridTabuleiro.addEventListener("click", selecaoDisco);
    botaoReiniciar.addEventListener("click", reiniciarJogo);
}

//Chamada da função que inicia o jogo
iniciarJogo();