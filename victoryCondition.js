//Função para verificar a vitória. Temos como entrada: o tabuleiro atualizado e a posição da
//ultima peça jogada. 
function verificarVitoria(tabuleiro, objJogada) {
    
    let contadorVitoria = 0;

    //Verifica a linha
    for (let i = 0; i < tabuleiro[objJogada.linha].length; i++) {
        if (tabuleiro[objJogada.linha][i] === tabuleiro[objJogada.linha][objJogada.coluna]) {
            contadorVitoria++;
        } else {
            contadorVitoria = 0;
        }
        if (contadorVitoria === 4) {
            return true;
        }
    }

    contadorVitoria = 0;


    //Verifica a coluna
    for (let i = 0; i < tabuleiro.length; i++) {
        if (tabuleiro[i][objJogada.coluna] === tabuleiro[objJogada.linha][objJogada.coluna]) {
            contadorVitoria++;
        } else {
            contadorVitoria = 0;
        }
        if (contadorVitoria === 4) {
            return true;
        }
    }

    contadorVitoria = 0;


    //Verifica diagonal1
    let colunaInicial;
    let linha;
    let coluna;
    if (objJogada.coluna > objJogada.linha) {
        linhaInicial = 0;
        colunaInicial = objJogada.coluna - objJogada.linha;
    } else {
        linhaInicial = objJogada.linha - objJogada.coluna;
        colunaInicial = 0;
    }
    linha = linhaInicial;
    coluna = colunaInicial;
    while (linha < tabuleiro.length && coluna < tabuleiro[0].length) {
        if (tabuleiro[linha][coluna] === tabuleiro[objJogada.linha][objJogada.coluna]) {
            contadorVitoria++;
        } else {
            contadorVitoria = 0;
        }
        if (contadorVitoria === 4) {
            return true;
        }
        linha++;
        coluna++;
    }

    contadorVitoria = 0;


    //Verifica diagonal2
    if (objJogada.coluna + objJogada.linha < tabuleiro.length) {
        linhaInicial = objJogada.linha + objJogada.coluna;
        colunaInicial = 0;
    } else {
        linhaInicial = tabuleiro.length-1;
        colunaInicial =objJogada.coluna -(tabuleiro.length - (objJogada.linha+1));
    }
    linha = linhaInicial;
    coluna = colunaInicial;
    while (linha >= 0 && coluna < tabuleiro[0].length) {
        if (tabuleiro[linha][coluna] === tabuleiro[objJogada.linha][objJogada.coluna]) {
            contadorVitoria++;
        } else {
            contadorVitoria = 0;
        }
        if (contadorVitoria === 4) {
            return true;
        }
        linha--;
        coluna++;
    }
    return false;
}

function testarTabuleiro() {
    // const exemploTabuleiro1 = [
    //     ["", "", "", "", "", "", ""],
    //     ["", "", "", "", "", "", ""],
    //     ["", "", "", "", "", "", "V"],
    //     ["", "", "", "", "", "V", ""],
    //     ["", "", "", "", "V", "", ""],
    //     ["", "", "", "V", "", "", ""]
    // ];

    // let objJogada = { linha: 3, coluna: 5 };
    // console.log(verificarVitoria(exemploTabuleiro1, objJogada));
}